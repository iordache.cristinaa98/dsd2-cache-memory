set moduleName cache_module
set isTopModule 1
set isCombinational 0
set isDatapathOnly 0
set isPipelined 0
set pipeline_type none
set FunctionProtocol ap_ctrl_hs
set isOneStateSeq 0
set ProfileFlag 0
set StallSigGenFlag 0
set isEnableWaveformDebug 1
set hasInterrupt 0
set C_modelName {cache_module}
set C_modelType { void 0 }
set C_modelArgList {
	{ address int 32 regular  }
	{ dirty int 1 regular {pointer 1}  }
	{ hit int 1 regular {pointer 1}  }
	{ data int 8 regular {pointer 1}  }
	{ updateSet uint 8 regular  }
	{ updateWay uint 8 regular  }
	{ newTag int 32 regular  }
	{ updateTagValid uint 1 regular  }
	{ newData int 8 regular {array 4 { 1 3 } 1 1 }  }
	{ updateDataValid uint 1 regular  }
}
set C_modelArgMapList {[ 
	{ "Name" : "address", "interface" : "wire", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "dirty", "interface" : "wire", "bitwidth" : 1, "direction" : "WRITEONLY"} , 
 	{ "Name" : "hit", "interface" : "wire", "bitwidth" : 1, "direction" : "WRITEONLY"} , 
 	{ "Name" : "data", "interface" : "wire", "bitwidth" : 8, "direction" : "WRITEONLY"} , 
 	{ "Name" : "updateSet", "interface" : "wire", "bitwidth" : 8, "direction" : "READONLY"} , 
 	{ "Name" : "updateWay", "interface" : "wire", "bitwidth" : 8, "direction" : "READONLY"} , 
 	{ "Name" : "newTag", "interface" : "wire", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "updateTagValid", "interface" : "wire", "bitwidth" : 1, "direction" : "READONLY"} , 
 	{ "Name" : "newData", "interface" : "memory", "bitwidth" : 8, "direction" : "READONLY"} , 
 	{ "Name" : "updateDataValid", "interface" : "wire", "bitwidth" : 1, "direction" : "READONLY"} ]}
# RTL Port declarations: 
set portNum 21
set portList { 
	{ ap_clk sc_in sc_logic 1 clock -1 } 
	{ ap_rst sc_in sc_logic 1 reset -1 active_high_sync } 
	{ ap_start sc_in sc_logic 1 start -1 } 
	{ ap_done sc_out sc_logic 1 predone -1 } 
	{ ap_idle sc_out sc_logic 1 done -1 } 
	{ ap_ready sc_out sc_logic 1 ready -1 } 
	{ address sc_in sc_lv 32 signal 0 } 
	{ dirty sc_out sc_logic 1 signal 1 } 
	{ dirty_ap_vld sc_out sc_logic 1 outvld 1 } 
	{ hit sc_out sc_logic 1 signal 2 } 
	{ hit_ap_vld sc_out sc_logic 1 outvld 2 } 
	{ data sc_out sc_lv 8 signal 3 } 
	{ data_ap_vld sc_out sc_logic 1 outvld 3 } 
	{ updateSet sc_in sc_lv 8 signal 4 } 
	{ updateWay sc_in sc_lv 8 signal 5 } 
	{ newTag sc_in sc_lv 32 signal 6 } 
	{ updateTagValid sc_in sc_logic 1 signal 7 } 
	{ newData_address0 sc_out sc_lv 2 signal 8 } 
	{ newData_ce0 sc_out sc_logic 1 signal 8 } 
	{ newData_q0 sc_in sc_lv 8 signal 8 } 
	{ updateDataValid sc_in sc_logic 1 signal 9 } 
}
set NewPortList {[ 
	{ "name": "ap_clk", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "clock", "bundle":{"name": "ap_clk", "role": "default" }} , 
 	{ "name": "ap_rst", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "reset", "bundle":{"name": "ap_rst", "role": "default" }} , 
 	{ "name": "ap_start", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "start", "bundle":{"name": "ap_start", "role": "default" }} , 
 	{ "name": "ap_done", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "predone", "bundle":{"name": "ap_done", "role": "default" }} , 
 	{ "name": "ap_idle", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "done", "bundle":{"name": "ap_idle", "role": "default" }} , 
 	{ "name": "ap_ready", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "ready", "bundle":{"name": "ap_ready", "role": "default" }} , 
 	{ "name": "address", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "address", "role": "default" }} , 
 	{ "name": "dirty", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "dirty", "role": "default" }} , 
 	{ "name": "dirty_ap_vld", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "dirty", "role": "ap_vld" }} , 
 	{ "name": "hit", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "hit", "role": "default" }} , 
 	{ "name": "hit_ap_vld", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "hit", "role": "ap_vld" }} , 
 	{ "name": "data", "direction": "out", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "data", "role": "default" }} , 
 	{ "name": "data_ap_vld", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "data", "role": "ap_vld" }} , 
 	{ "name": "updateSet", "direction": "in", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "updateSet", "role": "default" }} , 
 	{ "name": "updateWay", "direction": "in", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "updateWay", "role": "default" }} , 
 	{ "name": "newTag", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "newTag", "role": "default" }} , 
 	{ "name": "updateTagValid", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "updateTagValid", "role": "default" }} , 
 	{ "name": "newData_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":2, "type": "signal", "bundle":{"name": "newData", "role": "address0" }} , 
 	{ "name": "newData_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "newData", "role": "ce0" }} , 
 	{ "name": "newData_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "newData", "role": "q0" }} , 
 	{ "name": "updateDataValid", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "updateDataValid", "role": "default" }}  ]}

set RtlHierarchyInfo {[
	{"ID" : "0", "Level" : "0", "Path" : "`AUTOTB_DUT_INST", "Parent" : "", "Child" : ["1", "2"],
		"CDFG" : "cache_module",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "0", "ap_idle" : "1", "real_start" : "0",
		"Pipeline" : "None", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "0",
		"VariableLatency" : "1", "ExactLatency" : "-1", "EstimateLatencyMin" : "4", "EstimateLatencyMax" : "14",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "0",
		"HasNonBlockingOperation" : "0",
		"IsBlackBox" : "0",
		"Port" : [
			{"Name" : "address", "Type" : "None", "Direction" : "I"},
			{"Name" : "dirty", "Type" : "Vld", "Direction" : "O"},
			{"Name" : "hit", "Type" : "Vld", "Direction" : "O"},
			{"Name" : "data", "Type" : "Vld", "Direction" : "O"},
			{"Name" : "updateSet", "Type" : "None", "Direction" : "I"},
			{"Name" : "updateWay", "Type" : "None", "Direction" : "I"},
			{"Name" : "newTag", "Type" : "None", "Direction" : "I"},
			{"Name" : "updateTagValid", "Type" : "None", "Direction" : "I"},
			{"Name" : "newData", "Type" : "Memory", "Direction" : "I",
				"SubConnect" : [
					{"ID" : "2", "SubInstance" : "grp_cache_module_Pipeline_VITIS_LOOP_11_1_fu_198", "Port" : "newData", "Inst_start_state" : "3", "Inst_end_state" : "4"}]},
			{"Name" : "updateDataValid", "Type" : "None", "Direction" : "I"},
			{"Name" : "cacheMemory", "Type" : "Memory", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "2", "SubInstance" : "grp_cache_module_Pipeline_VITIS_LOOP_11_1_fu_198", "Port" : "cacheMemory", "Inst_start_state" : "3", "Inst_end_state" : "4"}]}]},
	{"ID" : "1", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.cacheMemory_U", "Parent" : "0"},
	{"ID" : "2", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.grp_cache_module_Pipeline_VITIS_LOOP_11_1_fu_198", "Parent" : "0", "Child" : ["3"],
		"CDFG" : "cache_module_Pipeline_VITIS_LOOP_11_1",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "0", "ap_idle" : "1", "real_start" : "0",
		"Pipeline" : "None", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "0",
		"VariableLatency" : "1", "ExactLatency" : "-1", "EstimateLatencyMin" : "7", "EstimateLatencyMax" : "7",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "0",
		"HasNonBlockingOperation" : "0",
		"IsBlackBox" : "0",
		"Port" : [
			{"Name" : "newData", "Type" : "Memory", "Direction" : "I"},
			{"Name" : "or_ln11", "Type" : "None", "Direction" : "I"},
			{"Name" : "cacheMemory", "Type" : "Memory", "Direction" : "O"}],
		"Loop" : [
			{"Name" : "VITIS_LOOP_11_1", "PipelineType" : "UPC",
				"LoopDec" : {"FSMBitwidth" : "1", "FirstState" : "ap_ST_fsm_pp0_stage0", "FirstStateIter" : "ap_enable_reg_pp0_iter0", "FirstStateBlock" : "ap_block_pp0_stage0_subdone", "LastState" : "ap_ST_fsm_pp0_stage0", "LastStateIter" : "ap_enable_reg_pp0_iter2", "LastStateBlock" : "ap_block_pp0_stage0_subdone", "QuitState" : "ap_ST_fsm_pp0_stage0", "QuitStateIter" : "ap_enable_reg_pp0_iter2", "QuitStateBlock" : "ap_block_pp0_stage0_subdone", "OneDepthLoop" : "0", "has_ap_ctrl" : "1", "has_continue" : "0"}}]},
	{"ID" : "3", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.grp_cache_module_Pipeline_VITIS_LOOP_11_1_fu_198.flow_control_loop_pipe_sequential_init_U", "Parent" : "2"}]}


set ArgLastReadFirstWriteLatency {
	cache_module {
		address {Type I LastRead 0 FirstWrite -1}
		dirty {Type O LastRead -1 FirstWrite 7}
		hit {Type O LastRead -1 FirstWrite 7}
		data {Type O LastRead -1 FirstWrite 7}
		updateSet {Type I LastRead 0 FirstWrite -1}
		updateWay {Type I LastRead 0 FirstWrite -1}
		newTag {Type I LastRead 0 FirstWrite -1}
		updateTagValid {Type I LastRead 0 FirstWrite -1}
		newData {Type I LastRead 0 FirstWrite -1}
		updateDataValid {Type I LastRead 0 FirstWrite -1}
		cacheMemory {Type IO LastRead -1 FirstWrite -1}}
	cache_module_Pipeline_VITIS_LOOP_11_1 {
		newData {Type I LastRead 0 FirstWrite -1}
		or_ln11 {Type I LastRead 0 FirstWrite -1}
		cacheMemory {Type O LastRead -1 FirstWrite 2}}}

set hasDtUnsupportedChannel 0

set PerformanceInfo {[
	{"Name" : "Latency", "Min" : "4", "Max" : "14"}
	, {"Name" : "Interval", "Min" : "5", "Max" : "15"}
]}

set PipelineEnableSignalInfo {[
]}

set Spec2ImplPortList { 
	address { ap_none {  { address in_data 0 32 } } }
	dirty { ap_vld {  { dirty out_data 1 1 }  { dirty_ap_vld out_vld 1 1 } } }
	hit { ap_vld {  { hit out_data 1 1 }  { hit_ap_vld out_vld 1 1 } } }
	data { ap_vld {  { data out_data 1 8 }  { data_ap_vld out_vld 1 1 } } }
	updateSet { ap_none {  { updateSet in_data 0 8 } } }
	updateWay { ap_none {  { updateWay in_data 0 8 } } }
	newTag { ap_none {  { newTag in_data 0 32 } } }
	updateTagValid { ap_none {  { updateTagValid in_data 0 1 } } }
	newData { ap_memory {  { newData_address0 mem_address 1 2 }  { newData_ce0 mem_ce 1 1 }  { newData_q0 mem_dout 0 8 } } }
	updateDataValid { ap_none {  { updateDataValid in_data 0 1 } } }
}

set maxi_interface_dict [dict create]

# RTL port scheduling information:
set fifoSchedulingInfoList { 
}

# RTL bus port read request latency information:
set busReadReqLatencyList { 
}

# RTL bus port write response latency information:
set busWriteResLatencyList { 
}

# RTL array port load latency information:
set memoryLoadLatencyList { 
}
