############################################################
## This file is generated automatically by Vitis HLS.
## Please DO NOT edit it.
## Copyright 1986-2022 Xilinx, Inc. All Rights Reserved.
############################################################
open_project dsd2-cache-memory
set_top cache_module
add_files dsd2-cache-memory/mem-example.hpp
add_files dsd2-cache-memory/mem-example.cpp
add_files -tb dsd2-cache-memory/tb_cache2.cpp -cflags "-Wno-unknown-pragmas" -csimflags "-Wno-unknown-pragmas"
open_solution "solution2" -flow_target vivado
set_part {xc7z020-clg400-1}
create_clock -period 10 -name default
source "./dsd2-cache-memory/solution2/directives.tcl"
csim_design
csynth_design
cosim_design
export_design -format ip_catalog
