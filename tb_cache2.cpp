#include <cstdint>
#include <iostream>
#include "mem-example.hpp"

int main(){
	bool dirty, hit = false;
	uint8_t data;
	uint8_t updateSet = 0;
	uint8_t updateWay = 0;
	uint32_t tag = 0x00250c2;
	//Addr: 0x01206111 -> 1,2,3,4

	uint8_t dataToWrite[DATA_SIZE] = {1,2,3,4};
	uint8_t secondDataToWrite[DATA_SIZE] = {5,6,7,8};
	uint8_t thirdDataToWrite[DATA_SIZE] = {7,7,3,3};

	//write to set 0, cache line 0
	cache_module({}, dirty, hit, data, updateSet, updateWay, tag, true, dataToWrite, true);

	updateWay = 1;
	tag = 0x10250c2;
	//write to set 0, cache line 1
	cache_module({}, dirty, hit, data, updateSet, updateWay, tag, true, secondDataToWrite, true);

	updateSet = 1;
	updateWay = 2;
	tag = 0x0909090;
	//write to set 1, cache line 2
	cache_module({}, dirty, hit, data, updateSet, updateWay, tag, true, thirdDataToWrite, true);

	//get 3rd element from set0, cache line 0
	cache_module({0x00250c2, 0, 2}, dirty, hit, data, 0, 0, 0, false, {}, false);
	std::cout<<"Searching for data in cache..."<<std::endl;
	if(hit && data == 3){
		std::cout<<"Success, third element from set 0 and cache line 0."<<std::endl;
	} else{
		std::cout<<"Fail!"<<std::endl;
	}

	//get first element from set1, cache line 2
	cache_module({0x0909090, 1, 1}, dirty, hit, data, 0, 0, 0, false, {}, false);
	if(hit && data == 7){
		std::cout<<"Success, first element from set 1 and cache line 2."<<std::endl;
	} else{
		std::cout<<"Fail!"<<std::endl;
	}


	return 0;

}
