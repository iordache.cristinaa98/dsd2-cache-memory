#include <cstdint>
#include <iostream>
#include "mem-example.hpp"

int main(){
	bool dirty, hit = false;
	uint8_t data;
	//Addr: 0x12121210 -> 1,2,3,4

	uint8_t dataToWrite[DATA_SIZE] = {1,2,3,4};
	uint8_t secondDataToWrite[DATA_SIZE] = {5,6,7,8};

	cache_module({}, dirty, hit, data, 4, 0, 0x0909090, true, dataToWrite, true);


	cache_module({}, dirty, hit, data, 3, 0, 0x0909090, true, secondDataToWrite, true);

	cache_module({0x0909090, 3, 1}, dirty, hit, data, 0, 0, 0, false, {}, false);
	if(hit && data == 6){
		std::cout<<"Success, hit!"<<std::endl;
	} else{
		std::cout<<"Fail!"<<std::endl;
	}


	cache_module({0x0909080, 3, 1}, dirty, hit, data, 0, 0, 0, false, {}, false);
	if(!hit){
		std::cout<<"Success, miss!"<<std::endl;
	} else{
		std::cout<<"Fail!"<<std::endl;
	}


	return 0;

}
