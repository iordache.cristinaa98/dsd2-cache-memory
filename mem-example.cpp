#include "ap_int.h"
#include <cstdint>
#include <iostream>
#include "mem-example.hpp"

static CacheLine cacheMemory[SETS][WAYS]{};
void cache_module(Address address, bool& dirty, bool& hit, uint8_t& data, uint8_t updateSet, uint8_t updateWay, uint32_t newTag, bool updateTagValid, uint8_t newData[DATA_SIZE], bool updateDataValid){
	if(updateDataValid){
		cacheMemory[updateSet][updateWay].isValid  = true;
        cacheMemory[updateSet][updateWay].isDirty = false;
	    for(int i = 0; i < DATA_SIZE; i++) cacheMemory[updateSet][updateWay].data[i] =  newData[i];
	}

	if(updateTagValid){
    	cacheMemory[updateSet][updateWay].tag = newTag;
    	cacheMemory[updateSet][updateWay].isValid  = true;
        cacheMemory[updateSet][updateWay].isDirty = false;
	}

	for(int i = 0; i < WAYS; i++){
		if(cacheMemory[address.index][i].isValid && cacheMemory[address.index][i].tag == address.tag){
			hit = true;
			data = cacheMemory[address.index][i].data[address.offset];
			dirty = cacheMemory[address.index][i].isDirty;
			return;
		}
	}
	hit = false;
}


