############################################################
## This file is generated automatically by Vitis HLS.
## Please DO NOT edit it.
## Copyright 1986-2022 Xilinx, Inc. All Rights Reserved.
############################################################
open_project dsd2-cache-memory
set_top cache_module
add_files dsd2-cache-memory/mem-example.cpp
add_files dsd2-cache-memory/mem-example.hpp
add_files -tb dsd2-cache-memory/tb_cache2.cpp
open_solution "solution1" -flow_target vivado
set_part {xc7z020-clg400-1}
create_clock -period 10 -name default
set_clock_uncertainty 2
source "./dsd2-cache-memory/solution1/directives.tcl"
csim_design
csynth_design
cosim_design
export_design -format ip_catalog
