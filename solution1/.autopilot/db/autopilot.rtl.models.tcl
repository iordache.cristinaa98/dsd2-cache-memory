set SynModuleInfo {
  {SRCNAME cache_module_Pipeline_VITIS_LOOP_11_1 MODELNAME cache_module_Pipeline_VITIS_LOOP_11_1 RTLNAME cache_module_cache_module_Pipeline_VITIS_LOOP_11_1
    SUBMODULES {
      {MODELNAME cache_module_flow_control_loop_pipe_sequential_init RTLNAME cache_module_flow_control_loop_pipe_sequential_init BINDTYPE interface TYPE internal_upc_flow_control INSTNAME cache_module_flow_control_loop_pipe_sequential_init_U}
    }
  }
  {SRCNAME cache_module_Pipeline_VITIS_LOOP_20_2 MODELNAME cache_module_Pipeline_VITIS_LOOP_20_2 RTLNAME cache_module_cache_module_Pipeline_VITIS_LOOP_20_2}
  {SRCNAME cache_module MODELNAME cache_module RTLNAME cache_module IS_TOP 1
    SUBMODULES {
      {MODELNAME cache_module_cacheMemory_RAM_AUTO_1R1W RTLNAME cache_module_cacheMemory_RAM_AUTO_1R1W BINDTYPE storage TYPE ram IMPL auto LATENCY 2 ALLOW_PRAGMA 1}
    }
  }
}
