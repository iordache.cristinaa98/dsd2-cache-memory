#pragma once

#define OFFSET_BITCOUNT 2
#define INDEX_BITCOUNT 3
#define WAYS 1


#define TAG_BITCOUNT (32-INDEX_BITCOUNT-OFFSET_BITCOUNT)
#define DATA_SIZE (1<<OFFSET_BITCOUNT)
#define SETS (1<<INDEX_BITCOUNT)

struct __attribute__((packed)) Address{
	uint32_t tag:TAG_BITCOUNT;
	uint8_t index:INDEX_BITCOUNT;
	uint8_t offset:OFFSET_BITCOUNT;
};

struct __attribute__((packed)) CacheLine{
	bool isValid:1;
	bool isDirty:1;
	uint32_t tag:TAG_BITCOUNT;
	uint8_t data[DATA_SIZE];
};

void cache_module(Address address, bool& dirty, bool& hit, uint8_t& data, uint8_t updateSet, uint8_t updateWay, uint32_t newTag, bool updateTagValid, uint8_t newData[DATA_SIZE], bool updateDataValid);
